<?php

namespace app\admin\controller;
use think\Config;

class Expert extends Admin
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //标题搜索
        $name = input('name');
        if(!empty($name)) {
            $where['name'] = ['like',"%$name%"];
        }

        $is_show = input('is_show');
        if(!empty($is_show)) {
            $where['is_show'] = $is_show;
        }

        $where['id'] = ['>',0];

        $lists = \think\Db::table('channels')
            ->where($where)
            ->order('id desc')
            ->paginate(20,false,[
                'query'     => [
                    'name' => $name,
                    'is_show' => $is_show
                ],
            ]);

        $experts = $lists->toArray();

        $page = $lists->render();
        $total = $lists->total();

        return view('index',['lists' => $experts['data'],'page' => $page,'total' => $total]);
    }

    /**
     * 添加专家
     * @return \think\response\View
     */
    public function add()
    {
        return view('add');
    }

    /**
     * 专家添加提交
     * @return \think\response\Json
     */
    public function addPost()
    {
        $validate = new \think\Validate([
            'name| 名字' => 'require',
            'introduction| 简介' => 'require',
            'file_url| 头像' => 'require',
        ]);
        $request_data = input();

        if (!$validate->check($request_data)) {
            return error($validate->getError());
        }

        $info = \think\Db::table('channels')
            ->where('name', $request_data['name'])
            ->find();
        if(!empty($info)) {
            return error('专家名字重复');
        }

        $data = [
            'name' => $request_data['name'],
            'desc' => $request_data['introduction'],
            'avatar' => $request_data['file_url'],
            'cover_image' => $request_data['cover_url'],
            'created_at' => date('Y-m-d H:i:s',time()),
            'updated_at' => date('Y-m-d H:i:s',time()),
        ];
        \think\Db::table('channels')->insert($data);
        writeAdminLog(session('VideoAdmin.name'),' 新增专家 ' . $request_data['name']); //后台管理日志
        return success('添加成功！');
    }

    /**
     * 编辑视频
     * @param $id
     * @return \think\response\View
     */
    public function edit($id)
    {
        $info = \think\Db::table('channels')->where('id',$id)->find();
        return view('edit',['info' => $info]);
    }

    /**
     * 提交编辑资料
     * @return \think\response\Json
     */
    public function editPost()
    {
        $validate = new \think\Validate([
            'name| 名字' => 'require',
            'introduction| 简介' => 'require',
            'file_url| 头像' => 'require',
        ]);
        $request_data = input();

        if (!$validate->check($request_data)) {
            return error($validate->getError());
        }

        $data = [
            'name' => $request_data['name'],
            'desc' => $request_data['introduction'],
            'avatar' => $request_data['file_url'],
            'cover_image' => $request_data['cover_url'],
            'updated_at' => date('Y-m-d H:i:s',time()),
        ];
        \think\Db::table('channels')->where('id',$request_data['id'])->update($data);

        writeAdminLog(session('VideoAdmin.name'),' 编辑专家 ' . $request_data['name']); //后台管理日志
        return success('编辑成功！');
    }

    /**
     * 上传头像
     */
    public function uploadFile()
    {
        $file = request()->file('file');
        if (empty($file)) {
            return "图片错误！";
        } else {
            $info = $file->getInfo();

            if ($info) {

                $object = 'avatar/' . $info['name'];

                $pictureName = getOssUrl($info['tmp_name'],$object,'avatar',2);

            } else {
                echo $file->getError();
            }
        }
        return success('上传成功',['url' => "https://".Config::get('OSS.OSS_URL').'/'.$pictureName]);
    }


    /**
     * 更改状态
     */
    public function changeStatus()
    {
        $field = input('field');
        $id = input('id');
        $type = input('type');

        switch ($type){
            case 'expert'://视频专栏
                $info = \think\Db::table('channels')
                    ->where('id',$id)
                    ->field("$field,name")
                    ->find();

                $status = $info[$field] == 1? 2 : 1;

                \think\Db::table('channels')
                    ->where('id',$id)
                    ->update(["$field" => "$status"]);

                writeAdminLog(session('VideoAdmin.name'),' 编辑专家 ' . $info['name'] . ' 状态'); //后台管理日志
                break;
        }
    }


    /**
     * 删除
     */
    public function deleted()
    {
        $type = input('type');
        $id = input('id');

        switch ($type) {
            case 'expert': //专家管理
                \think\Db::table('channels')
                    ->where('id',$id)
                    ->delete();

                writeAdminLog(session('VideoAdmin.name'),' 删除专家ID：' . $id); //后台管理日志
                break;

        }
        return success('成功删除');
    }


}
